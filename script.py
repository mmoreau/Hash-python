import os
import sys
from Hash import Hash


class App:



	def __init__(self):
		pass



	@classmethod
	def Way(cls):

		""" Basic function to acquire the basics of the Hash module. """


		# Displays the supported hash algorithms of the script
		print(Hash.Supported_Algorithm(), "\n")

		password = Hash.Hash("P4$$w0rd!", "sha256")
		file = Hash.File("file/file.txt", "sha256")
		checkPassword = Hash.Equal(password, "5adaa541cd53722c0a84fa7d37e6ca0e016cc8a9fa934bfdf6fb9288e6d6b1c0")

		print("P4$$w0rd! : " + password)
		print("file/file.txt : " + file, "\n")

		# Displays only if it is correct or not
		print(["Bad 1", "Good 1"][checkPassword])

		# Check if it is correct
		if checkPassword:
			print("Good 2 ")

		if not checkPassword:
			print("Bad 2")



	@classmethod
	def Way2(cls):

		""" 
			This function shows you an example of how we calculate checksums for each file in 
			a single (non-recursive) folder, you can specify the files to which you do not want 
			to calculate checksum. 
		"""

		path = "file"

		for file in os.listdir(path):
			if file not in "SHA256SUMS.txt":
				abspath = os.path.abspath(path) + "\\" + file
				sys.stdout.write("{} : {}\n".format(file, Hash.File(abspath, "sha256")))



	@classmethod
	def Way3(cls):

		"""
			This function allows you to show you an example of how we check checksums for each file 
			in a single (non-recursive) folder, you can specify the files to which you do not want to check checksum.

			To retrieve the checksums from the files, the fingerprints are stored in the "file" directory of the "SHA256SUMS.txt" file.  

			Each file you browse in a folder, you check it by reading the file "SHA256SUMS.txt", 
			to know if the file is correct or corrumpus.
		"""


		# We'll get the prints from the file "SHA256SUMS.txt"

		data = {}

		with open("file/SHA256SUMS.txt", "r") as f:
			for l in f.readlines():
				dataFileSplit = l.split(" ")
				data[dataFileSplit[0]] = dataFileSplit[1].split("\n")[0]


		# Scans the files in the folder and checks if it is correct

		path = "file"

		for file in os.listdir(path):

			if file not in "SHA256SUMS.txt":

				abspath = os.path.abspath(path) + "\\" + file
				hashFile = Hash.File(abspath, "sha256")

				if isinstance(hashFile, str):

					hashEqual = Hash.Equal(hashFile, data[file])

					if isinstance(hashEqual, bool):
						sys.stdout.write("{} -> {}\n".format(abspath, ["Bad", "Good"][hashEqual]))



	@classmethod
	def Way4(cls):

		""" Displays the footprints of the passwords stored in a table.  """

		password_list = ["P4$$w0rd!", "(-)3110", "qwerty123", "123456"]

		# Runs each element of the array, the elements here are passwords 
		for password in password_list:

			# Hash password calculation
			passwordHash = Hash.Hash(password, "sha256")

			if isinstance(passwordHash, str):
				sys.stdout.write("{} -> {}\n".format(password, passwordHash))



	@classmethod
	def Way5(cls):

		password_list = ["P4$$w0rd!", "(-)3110", "qwerty123", "123456"]

		# Runs each element of the array, the elements here are passwords 
		for password in password_list:

			sys.stdout.write(".:: {} ::.\n\n\n".format(password))

			for algorithm in Hash.Supported_Algorithm():

			# Hash password calculation
				passwordHash = Hash.Hash(password, algorithm)

				if isinstance(passwordHash, str):
					sys.stdout.write("\t{} -> {}\n".format(algorithm.rjust(8), passwordHash))

			sys.stdout.write("\n\n{}\n\n\n".format("-"*50))


App.Way()
#App.Way2()
#App.Way3()
#App.Way4()
#App.Way5()


if sys.platform.startswith('win32'):
	os.system("pause")