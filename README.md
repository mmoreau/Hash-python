<h1>Hash</h1>
<h3>What is a hash ?</h3>
<ul>
    <li>https://en.wikipedia.org/wiki/Hash_function</li>
</ul>

<h3>What is a checksum?</h3>
<ul>
    <li>https://en.wikipedia.org/wiki/Checksum</li>
</ul>

<h2>Terminal</h2>

<h3><b>Windows</b></h3>

<p>Displays the list of supported hash algorithms in this script</p>
<pre>
    Hash.py -l
</pre>

<p>Displays the hash of a character string specifying the hash algorithm you want to use</p>
<pre>
    Hash.py -s hello -c sha512
</pre>

<p>Displays the checksum of a file specifying the hash algorithm you want to use</p>
<pre>
    Hash.py -f file/hello.py -c sha512
</pre>

<p>Checks the hash of a character string. Returns true if it is correct and false if it is not correct </p>
<pre>
    Hash.py -s hello -c sha256 -v 2CF24DBA5FB0A30E26E83B2AC5B9E29E1B161E5C1FA7425E73043362938B9824
</pre>

<p>Checks the checksum of a file. Returns true if it is correct and false if it is not correct</p>
<pre>
    Hash.py -f file/test.exe -c sha256 -v 5151327d5d4d822d354373205deaf9fefb43e21395841a9702abab955b7dcd30
</pre>

<h3><b>Linux</b></h3>
<p>Same command as Windows except that you have to change the script execution rights and put before"./Hash.py".</p>

<h4>Example</h4>
<pre>
    chmod +x Hash.py
    ./Hash.py -s hello -c sha512
</pre>

<h3><b>Mac</b></h3>
<p>Same command as Windows except that you have to change the script execution rights and put before "python3 Hash.py".</p>

<h4>Example</h4>
<pre>
    python3 Hash.py -s hello -c sha512
</pre>

<h2>Script</h2>
<p>Consult the "script.py" file to understand how the Hexdump module works</p>
<h3><b>Windows</b></h3> 
<p>Double click on the "script.py" file</p>
  
<h3><b>Linux</b></h3>
<p>Modify the execution rights then execute the script</p>
<pre>
    chmod +x script.py
    ./script.py
</pre>

<h3><b>Mac</b></h3>
<pre>
    python3 script.py
</pre>

<h2>Contact me</h2>
<p>Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered </p>

<p><b>Mail :</b> mx.moreau1@gmail.com<p>
<p><b>Twitter :</b> mxmmoreau (https://twitter.com/mxmmoreau)</p>