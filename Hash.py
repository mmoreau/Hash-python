import argparse
import hashlib
import os
import sys


_HASH_ALGO = ["md5", "sha1", "sha224", "sha256", "sha384", "sha512", "sha3_224", "sha3_256", "sha3_384", "sha3_512", "blake2b", "blake2s"]


class Hash:



	def __init__(self):
		pass



	@classmethod
	def Hash(cls, string, algorithm):

		"""Return the fingerprint of a character string."""

		algorithm = algorithm.lower()
		lock = 0

		# Check if the data is correct
		if isinstance(string, str):
			lock += 1


		if isinstance(algorithm, str):
			if algorithm in _HASH_ALGO:
				lock += 1


		if lock == 2:

			if algorithm == "md5":
				_hash = hashlib.md5(string.encode()).hexdigest()

			# .:: SHA-1 ::.

			if algorithm == "sha1":
				_hash = hashlib.sha1(string.encode()).hexdigest()

			# .:: SHA-2 ::.

			if algorithm == "sha224":
				_hash = hashlib.sha224(string.encode()).hexdigest()

			if algorithm == "sha256":
				_hash = hashlib.sha256(string.encode()).hexdigest()

			if algorithm == "sha384":
				_hash = hashlib.sha384(string.encode()).hexdigest()

			if algorithm == "sha512":
				_hash = hashlib.sha512(string.encode()).hexdigest()

			# .:: SHA-3 ::.

			if algorithm == "sha3_224":
				_hash = hashlib.sha3_224(string.encode()).hexdigest()

			if algorithm == "sha3_256":
				_hash = hashlib.sha3_256(string.encode()).hexdigest()

			if algorithm == "sha3_384":
				_hash = hashlib.sha3_384(string.encode()).hexdigest()

			if algorithm == "sha3_512":
				_hash = hashlib.sha3_512(string.encode()).hexdigest()

			# .:: Blake ::.

			if algorithm == "blake2b":
				_hash = hashlib.blake2b(string.encode()).hexdigest()

			if algorithm == "blake2s":
				_hash = hashlib.blake2s(string.encode()).hexdigest()

			return _hash


		if lock < 2:
			return None



	@classmethod
	def File(cls, filename, algorithm):

		"""Return the fingerprint of a file (checksum)."""

		filename = filename.lower()
		algorithm = algorithm.lower()
		lock = 0

		# Check if the data is correct
		if isinstance(filename, str):
			if os.path.isfile(filename):
				lock += 1
				

		if isinstance(algorithm, str):
			if algorithm in _HASH_ALGO:
				lock += 1


		if lock == 2:
					
			if algorithm == "md5":
				_hash = hashlib.md5()

			# .:: SHA-1 ::.

			if algorithm == "sha1":
				_hash = hashlib.sha1()

			# .:: SHA-2 ::.

			if algorithm == "sha224":
				_hash = hashlib.sha224()

			if algorithm == "sha256":
				_hash = hashlib.sha256()

			if algorithm == "sha384":
				_hash = hashlib.sha384()

			if algorithm == "sha512":
				_hash = hashlib.sha512()

			# .:: SHA-3 ::.

			if algorithm == "sha3_224":
				_hash = hashlib.sha3_224()

			if algorithm == "sha3_256":
				_hash = hashlib.sha3_256()

			if algorithm == "sha3_384":
				_hash = hashlib.sha3_384()

			if algorithm == "sha3_512":
				_hash = hashlib.sha3_512()

			# .:: Blake ::.

			if algorithm == "blake2b":
				_hash = hashlib.blake2b()

			if algorithm == "blake2s":
				_hash = hashlib.blake2s()


			# Checksum calculation
			with open(filename, 'rb') as f:
				for block in iter(lambda: f.read(65536), b""):
					_hash.update(block)

			return _hash.hexdigest()


		if lock < 2:
			return None



	@classmethod
	def Equal(cls, hash1, hash2):

		lock = 0

		if isinstance(hash1, str):
			lock += 1

		if isinstance(hash2, str):
			lock += 1

		if lock == 2:
			return bool(hash1.lower() == hash2.lower())

		if lock != 2:
			return None



	@classmethod
	def Supported_Algorithm(cls):

		"""Return the available algorithms of the Hash class."""

		return _HASH_ALGO



try:
	parser = argparse.ArgumentParser()

	parser.add_argument('-f', "--file", type=str)
	parser.add_argument('-s', "--string", type=str)
	parser.add_argument('-c', "--cipher", type=str)
	parser.add_argument('-v', "--verify", type=str)
	parser.add_argument('-l', "--list", action="store_true")

	args = parser.parse_args()

	_file = args.file
	_string = args.string
	_list = args.list
	_cipher =  args.cipher
	_verify = args.verify

	_lockVerify = False


	if _file and _cipher:

		_hash = Hash.File(_file, _cipher)

		if isinstance(_hash, str):

			if _verify:
				_lockVerify = True

			if not _verify:
				sys.stdout.write("{}".format(_hash))


	if _string and _cipher:
		
		_hash = Hash.Hash(_string, _cipher)

		if isinstance(_hash, str):

			if _verify:
				_lockVerify = True

			if not _verify:
				sys.stdout.write("{}".format(_hash))


	if _verify and _lockVerify:
		_check = Hash.Equal(_hash, _verify)

		if isinstance(_check, bool):
			sys.stdout.write("{}".format(_check))


	if _list:
		sys.stdout.write("\n\n.:: The hash algorithms supported by this script ::.\n\n")
		sys.stdout.write("{}".format('\n'.join(Hash.Supported_Algorithm())))
except:
	pass